import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native';
import { MainScreen } from './src/screens/MainScreen';
import { MathScreen } from './src/screens/MathScreen';

export default function App() {
const [deviceWidth, setDeviceWidth] = useState( Dimensions.get('window').width)

  useEffect(() => {
    const update = () => {
    const width = Dimensions.get('window').width 
      setDeviceWidth(width)
    }
    Dimensions.addEventListener('change', update)
  })   

  return (
    <View style={styles.container}>
      { deviceWidth < Dimensions.get('window').height ? <MainScreen /> : <MathScreen />}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Dimensions.get('window').height *0.25,
    paddingBottom: Dimensions.get('window').height *0.25 / 4,
    paddingHorizontal: 20
  },
})
