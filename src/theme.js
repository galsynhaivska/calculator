export const THEME = {
    GRAY_COLOR: '#373737',
    ORANGE_COLOR: '#a8a8a8',
    DIGIT_COLOR: '#404040',
    BLACK_COLOR: '#000000',
    WHITE_COLOR: '#ffffff',
    FONT_PORTRAIT: 36
}
