import React, {useState} from 'react'
import { Text, StyleSheet, View, Dimensions } from 'react-native';
import { DigitalButton } from '../components/ui/DigitalButton'

export const MathScreen = () => {
 const [number, setNumber] = useState('0')

  const addDigit = digit => {
    if(operation){
      setNumber('0')
       if(digit === '0'){
            setNumber('0')
          }else{
            setNumber(digit)
          }  
           operation = false
    }else{
      if(number.length < 10){
        if(number === '0'){
          if(digit === '0'){
            setNumber('0')
          }else{
            setNumber(digit)
          }  
        }else if( number === '-0'){
          setNumber('-' + digit)  
        }else{
          setNumber(number + digit)
        }
      }   
    }
  }

  const addComma = () =>{
    if(!operation){
      if(number.includes(',')){
        setNumber(number)
      }else{
        setNumber(number + ',')
      }
    }else{
      setNumber('0,')
      operation = false
    }
  }

  const setMinus = () => {
    if(number[0] !== '-'){
       setNumber('-' + number)
    }else{
      setNumber(number.slice(1))
    } 
  }

  const setPercent = () => {
    operation = true 
    let res = String(number.replace(',', '.') / 100).replace('.', ',')
    setNumber(res)
    signOperation = ''
    operand1 = res
  }
  const clearAll = () => {
    res=''
    operation = false
    equal = false
    signOperation = ''
    operand1 = ''
    setNumber('0')
  }

  const saveSign = sign => {  
    if(equal){
      operand1 = number
      equal = false
      operation = true
      signOperation = sign
    }else{
      if(signOperation === ''){
        signOperation = sign
        operand1 = number
      }else{
        actionDigits()
        signOperation = sign
      }
      operation = true
    }
  }

    const getResult = (operand1, number, signOperation) => {
      if(signOperation === '+'){  
        return parseFloat(operand1.replace(',', '.')) + parseFloat(number.replace(',', '.'))
      }else if(signOperation === '-'){
        return parseFloat(operand1.replace(',', '.')) - parseFloat(number.replace(',', '.'))
      }else if(signOperation === 'x'){
        return parseFloat(operand1.replace(',', '.')) * parseFloat(number.replace(',', '.'))
      }else if(signOperation === '/'){
        if(number === '0'){
          return 'ERROR'
        }else{
          return parseFloat(operand1.replace(',', '.')) / parseFloat(number.replace(',', '.'))
        }
      }
    }

  const actionDigits = () =>{
    if(operand1 === 'ERROR'){
      res = 'ERROR'
    }else if( operand1 !== ''){
      res = getResult( operand1, number, signOperation)  
    }
    operand1 = String(res).replace('.', ',')
    setNumber(String(res).replace('.', ',') )   
  }

  const saveEqual = () => {
    if(operand1 !== ''){  
      if(operand1 === 'ERROR'){
        res = 'ERROR'
      }else if(operand1 !== ''){
        res = getResult( operand1, number, signOperation)  
      }
    operand1 = String(res).replace('.', ',')
    setNumber(String(res).replace('.', ',') )  
    equal = true 
    operation = true
    }
  }

  return (    
    <View>
      <View style={styles.rowtext} >
        <Text style={styles.textbox} value={{number: number }}>{number}</Text>
      </View>
      <View style={styles.row}>
        <DigitalButton style={[styles.box, styles.box1]} onPress={()=>clearAll('AC')}>AC</DigitalButton>  
        <DigitalButton style={[styles.box, styles.box1]} onPress={()=>setMinus('+/-')}>+/-</DigitalButton>  
        <DigitalButton style={[styles.box, styles.box1]} onPress={()=>setPercent('%')}>%</DigitalButton>
        <DigitalButton style={[styles.box, styles.box3]} onPress={()=>saveSign('/')}>/</DigitalButton>  
      </View> 
      
      <View style={styles.row}>
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('7')}>7</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('8')}>8</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('9')}>9</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box3]} onPress={()=>saveSign('x')}>x</DigitalButton>  
      </View> 

      <View style={styles.row}>
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('4')}>4</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('5')}>5</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('6')}>6</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box3]} onPress={()=>saveSign('-')}>-</DigitalButton>    
      </View> 

      <View style={styles.row}>
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('1')}>1</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('2')}>2</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addDigit('3')}>3</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box3]} onPress={()=>saveSign('+')}>+</DigitalButton>  
      </View> 

       <View style={styles.row}>
        <DigitalButton style={[styles.box, styles.box0]} onPress={()=>addDigit('0')}>0</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box2]} onPress={()=>addComma(',')}>,</DigitalButton> 
        <DigitalButton style={[styles.box, styles.box3]} onPress={()=>saveEqual('=')}>=</DigitalButton> 
      </View> 
    </View>
  );
}

const styles = StyleSheet.create({
  rowtext: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 10,
  },
  textbox: {
    height: Dimensions.get('window').height / 10,
    width: Dimensions.get('window').width - 40,
    padding: 5,
    marginBottom: 25,
    backgroundColor: '#000',
    color: '#fff',
    fontSize: 72,
    fontWeight: '200',
    textAlign: 'right'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around', 
  },
  box: {
    flex: 1,
    height: Dimensions.get('window').height *0.25 *0.36,
    marginHorizontal: 5,
    borderRadius: 50, 
    textAlign: 'center', 
    fontSize: 44,
  },
  box0: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#404040',
  
  },
  box1: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#a8a8a8',
  },
  
  box2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#373737',
    color: '#000',
  },

  box3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'orange',
    color: '#fff',
  },
  
});
