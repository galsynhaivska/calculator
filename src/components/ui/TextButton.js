import React from 'react'
import { Text} from 'react-native'


export const TextButton = props => {
  return (
        <Text style={props.style}>{props.children}</Text>
    )
}

