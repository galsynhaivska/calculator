import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import { TextButton } from './TextButton'
import { THEME } from '../../theme'

export const DigitalButton = ({ children, onPress, style }) => {
  return (
        <TouchableOpacity activeOpacity={0.5} onPress={onPress} style={style}>  
              <TextButton  style={styles.text2}>{children}</TextButton>
          </TouchableOpacity> 
    )
}

const styles = StyleSheet.create({
  text2:{
    color: THEME.WHITE_COLOR,
    fontSize: THEME.FONT_PORTRAIT,
    fontWeight: '400',
  },
});

